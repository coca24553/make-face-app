import 'dart:math';

import 'package:climbing_party_app/pages/page_make_face.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key
  });

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  TextEditingController tec = TextEditingController();

  int eyebrowNumber = 1;
  int eyesNumber = 1;
  int hairNumber = 1;
  int clothesNumber = 1;
  int mouseNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
            'Make Friend',
          style: TextStyle(
            fontFamily: 'font_03',
          ),
        ),
        backgroundColor: Colors.blue[400],
        foregroundColor: Colors.white,
      ),
      
      body:
      SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(100),
                    child: Column(
                      children: [
                        Text(
                            'H E L L O',
                          style: TextStyle(
                              fontFamily: 'font_02',
                            fontSize: 30,
                            color: Colors.blue[400],
                          ),
                        ),
                        Text(
                            'M Y',
                          style: TextStyle(
                            fontFamily: 'font_02',
                            fontSize: 30,
                            color: Colors.grey,
                          ),
                        ),
                        Text(
                            '★ F R I E N D ★',
                          style: TextStyle(
                            fontFamily: 'font_02',
                            fontSize: 30,
                            color: Colors.blue[400],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                TextField(
                                  style: TextStyle(
                                    fontFamily: 'font_03',
                                    color: Colors.black,
                                  ),
                                  controller: tec,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'name',
                                  ),
                                ),
                              ],
                            ),
                            width: 200,
                          ),
                          Container(
                            child: ElevatedButton(
                              style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.blue[400],
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                )
                              ),
                              onPressed: () {
                                Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => PageMakeFace(
                                    friendName: tec.text,
                                    )
                                  )
                                );
                              },
                              child: const Text(
                                  '시작하기',
                                style: TextStyle(
                                    fontFamily: 'font_03'
                                ),
                              ),
                            ),
                            margin: EdgeInsets.all(10),
                            width: 200,
                            height: 40,
                          ),
                        ]
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
