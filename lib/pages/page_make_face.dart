import 'dart:math';

import 'package:climbing_party_app/pages/page_index.dart';
import 'package:flutter/material.dart';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
    required this.friendName,
  });

  final String friendName;

  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {
  int eyebrowNumber = Random().nextInt(5) + 1;
  int eyesNumber = Random().nextInt(5) + 1;
  int hairNumber = Random().nextInt(5) + 1;
  int clothesNumber = Random().nextInt(5) + 1;
  int mouseNumber = Random().nextInt(5) + 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue[400],
        title: const Text(
          'Make Friend',
          style: TextStyle(
              fontFamily: 'font_03'
          ),
        ),
        foregroundColor: Colors.white,
      ),

      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/body.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/eyebrow/eyebrow$eyebrowNumber.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/eyes/eyes$eyesNumber.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/hair/hair$hairNumber.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/clothes/clothes$clothesNumber.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Image.asset(
                              'assets/mouse/mouse$mouseNumber.png',
                              width: 300,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Text(
                          widget.friendName,
                          style: TextStyle(
                            fontFamily: 'font_03',
                            fontSize: 20,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Container(
                          child: ElevatedButton(
                            style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.blue[400],
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                )
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => PageIndex()
                                ),
                              );
                            },
                            child: const Text(
                              '다시하기',
                              style: TextStyle(
                                  fontFamily: 'font_03'
                              ),
                            ),
                          ),
                          width: 200,
                          height: 40,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}